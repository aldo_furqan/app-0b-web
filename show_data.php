<?php

$DB_NAME = "hewanternak";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $jenis_hewan = $_POST['jenis_hewan'];
    $sql = "SELECT h.kd_hewan, h.jenis_hewan, p.jenis_pangan, concat('http://192.168.43.170/www/hewan/images/', h.photos) as url
            FROM hewan h, pangan p 
            WHERE h.id_pangan = p.id_pangan
            AND h.jenis_hewan LIKE '%$jenis_hewan%'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_hwn = array();
        while ($hwn = mysqli_fetch_assoc($result)) {
            array_push($data_hwn, $hwn);
        }
        echo json_encode($data_hwn);
    }
}
