<?php
  include('add.php')
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pendataan Ternak</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
      <div class="container">
        <a class="navbar-brand" href="index.php">Pendataan Ternak</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto"></ul>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a class="btn btn-outline-danger" href="index.php"><i class="fa fa-sign-out-alt"></i></a></li>
            </ul>
        </div>
      </div>
    </nav>

      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">Buat Data Hewan Ternak</div>
              <div class="card-body">
                <form class="" action="add.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="kd_hewan">Kode Hewan</label>
                      <input type="text" class="form-control" name="kd_hewan"  placeholder="Masukkan kode" value="">
                    </div>
                    <div class="form-group">
                      <label for="jenis_hewan">Jenis Hewan Ternak</label>
                      <input type="text" class="form-control" name="jenis_hewan"  placeholder="Masukkan jenis hewan" value="">
                    </div>
                    <div class="form-group">
                      <label for="id_pangan">Jenis Pangan</label>
                        <select class="form-control" name="id_pangan" id="id_pangan">
                            <option value="">- Pilih Pangan</option>
                            <?php
                              $sql = "select * from pangan order by jenis_pangan asc";
                              $result = mysqli_query($conn, $sql);
                              while ($data = mysqli_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $data['id_pangan'];?>"><?php echo $data['jenis_pangan'];?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <!-- <div class="form-group">
                      <label for="id_prodi">Prodi</label>
                      <input type="text" class="form-control" name="id_prodi" placeholder="Masukkan prodi" value="">
                    </div> -->
                    <div class="form-group">
                      <label for="photos">Pilih Foto</label>
                      <input type="file" class="form-control" name="photos" value="">
                    </div>
                    <div class="form-group">
                      <button type="submit" name="Submit" class="btn btn-primary waves">Submit</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    <script src="js/bootstrap.min.js" charset="utf-8"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" charset="utf-8"></script>
  </body>
</html>
