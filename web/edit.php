<?php
  require_once('db.php');
  $upload_dir = '../images/';

  if (isset($_GET['kd_hewan'])) {
    $kd_hewan = $_GET['kd_hewan'];
    $sql = "select * from hewan where kd_hewan=".$kd_hewan;
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_assoc($result);
    }else {
      $errorMsg = 'Tidak ada data';
    }
  }

  if(isset($_POST['Submit'])){
		$kd_hewan = $_POST['kd_hewan'];
		$jenis_hewan = $_POST['jenis_hewan'];
    $id_pangan = $_POST['id_pangan'];
    
    $sql = "select * from pangan where id_pangan = '$_GET[id_pangan]' order by jenis_pangan asc";
    $result = mysqli_query($conn, $sql);

		$imgName = $_FILES['photos']['name'];
		$imgTmp = $_FILES['photos']['tmp_name'];
		$imgSize = $_FILES['photos']['size'];

		if($imgName){

			$imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

			$allowExt  = array('jpeg', 'jpg', 'png', 'gif');

			$userPic = time().'_'.rand(1000,9999).'.'.$imgExt;

			if(in_array($imgExt, $allowExt)){

				if($imgSize < 5000000){
					unlink($upload_dir.$row['photos']);
					move_uploaded_file($imgTmp ,$upload_dir.$userPic);
				}else{
					$errorMsg = 'Ukuran foto terlalu besar';
				}
			}else{
				$errorMsg = 'File foto tidak tepat';
			}
		}else{

			$userPic = $row['photos'];
		}

		if(!isset($errorMsg)){
			$sql = "update hewan
									set jenis_hewan = '".$jenis_hewan."',
										id_pangan = '".$id_pangan."',
										photos = '".$userPic."'
					    where kd_hewan=".$kd_hewan;
			$result = mysqli_query($conn, $sql);
			if($result){
				$successMsg = 'Data berhasil diupdate';
				header('Location:index.php');
			}else{
				$errorMsg = 'Error '.mysqli_error($conn);
			}
		}

	}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pendataan Ternak</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
      <div class="container">
        <a class="navbar-brand" href="index.php">Pendataan Ternak</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto"></ul>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a class="btn btn-outline-danger" href="index.php"><i class="fa fa-sign-out-alt"></i></a></li>
            </ul>
        </div>
      </div>
    </nav>

      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                Udah Pendataan
              </div>
              <div class="card-body">
                <form class="" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="kd_hewan">Kode Hewan</label>
                      <input type="text" class="form-control" name="kd_hewan"  placeholder="Masukkan kode hewan value="<?php echo $row['kd_hewan']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="jenis_hewan">Jenis Hewan Ternak</label>
                      <input type="text" class="form-control" name="jenis_hewan"  placeholder="Masukkan jenis hewan" value="<?php echo $row['jenis_hewan']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="id_pangan">Jenis Pangan</label>
                      <input type="text" class="form-control" name="jenis_pangan"  placeholder="Masukkan jenis pangan" value="<?php echo $row['jenis_pangan']; ?>"> 
                    </div>
                    <div class="form-group">
                      <label for="photos">Pilih Foto</label>
                      <div class="col-md-4">
                        <img src="<?php echo $upload_dir.$row['photos'] ?>" width="100">
                        <input type="file" class="form-control" name="photos" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <button type="submit" name="Submit" class="btn btn-primary waves">Submit</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script src="js/bootstrap.min.js" charset="utf-8"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" charset="utf-8"></script>
  </body>
</html>
