<?php
  	require_once('db.php');
  	$upload_dir = '../images/';

  	
  	if (isset($_POST['Submit'])) {
    	$kd_hewan = $_POST['kd_hewan'];
    	$jenis_hewan = $_POST['jenis_hewan'];
    	$id_pangan = $_POST['id_pangan'];
		
		$sql = "select * from pangan order by jenis_pangan asc";
        $result = mysqli_query($conn, $sql);

    	$imgName = $_FILES['photos']['name'];
		$imgTmp = $_FILES['photos']['tmp_name'];
		$imgSize = $_FILES['photos']['size'];

    	if(empty($kd_hewan)){
			$errorMsg = 'Mohon masukkan nim';
		}elseif(empty($jenis_hewan)){
			$errorMsg = 'Mohon masukkan nama';
		}elseif(empty($id_pangan)){
			$errorMsg = 'Mohon pilih prodi';
		}else{

			$imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

			$allowExt  = array('jpeg', 'jpg', 'png', 'gif');

			$userPic = time().'_'.rand(1000,9999).'.'.$imgExt;

			if(in_array($imgExt, $allowExt)){

				if($imgSize < 5000000){
					move_uploaded_file($imgTmp ,$upload_dir.$userPic);
				}else{
					$errorMsg = 'Ukuran foto terlalu besar';
				}
			}else{
				$errorMsg = 'File foto tidak tepat';
			}
		}


		if(!isset($errorMsg)){
			$data = mysqli_fetch_assoc($result);
            $id_pangan = $data['id_pangan'];
			$sql = "insert into hewan(kd_hewan, jenis_hewan, id_pangan, photos)
					values('".$kd_hewan."', '".$jenis_hewan."', '".$id_pangan."', '".$userPic."')";
			$result = mysqli_query($conn, $sql);
			if($result){
				$successMsg = 'Data berhasil diinput';
				header('Location: index.php');
			}else{
				$errorMsg = 'Error '.mysqli_error($conn);
			}
		}
  }
?>
