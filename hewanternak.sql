-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Bulan Mei 2020 pada 17.01
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hewanternak`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hewan`
--

CREATE TABLE `hewan` (
  `kd_hewan` varchar(16) NOT NULL,
  `jenis_hewan` varchar(32) NOT NULL,
  `id_pangan` int(11) NOT NULL,
  `photos` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hewan`
--

INSERT INTO `hewan` (`kd_hewan`, `jenis_hewan`, `id_pangan`, `photos`) VALUES
('T-1000', 'suro', 2, 'DC_20200514191911.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pangan`
--

CREATE TABLE `pangan` (
  `id_pangan` int(11) NOT NULL,
  `jenis_pangan` varchar(32) NOT NULL,
  `merek` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pangan`
--

INSERT INTO `pangan` (`id_pangan`, `jenis_pangan`, `merek`) VALUES
(1, 'rumput', 'rajawali'),
(2, 'dedak', 'pak amir'),
(3, 'pelet', 'perahu sawah'),
(4, 'jagung', 'jagung rembulan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `hewan`
--
ALTER TABLE `hewan`
  ADD PRIMARY KEY (`kd_hewan`);

--
-- Indeks untuk tabel `pangan`
--
ALTER TABLE `pangan`
  ADD PRIMARY KEY (`id_pangan`),
  ADD KEY `id_pangan` (`id_pangan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pangan`
--
ALTER TABLE `pangan`
  MODIFY `id_pangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
